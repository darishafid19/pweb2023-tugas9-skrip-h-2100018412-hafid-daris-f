<?php
    $gaji = 1000000;
    $pajak = 0.1;
    $thp = $gaji - ($gaji*$pajak);

    $a = 5;
    $b = 4;
?>

<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            background-color: #f2f2f2;
            font-family: Arial, sans-serif;
        }

        .container {
            margin: 50px auto;
            padding: 20px;
            width: 300px;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }

        h1 {
            color: #333;
            text-align: center;
        }

        .result {
            margin-top: 20px;
            padding: 10px;
            background-color: #e6e6e6;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .label {
            font-weight: bold;
            color: #333;
        }

        .value {
            color: #555;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Perhitungan Gaji</h1>

        <div class="result">
            <span class="label">Gaji sebelum pajak:</span>
            <span class="value">Rp. <?php echo $gaji; ?></span>
            <br>
            <span class="label">Gaji yang dibawa pulang:</span>
            <span class="value">Rp. <?php echo $thp; ?></span>
        </div>
    </div>

    <div class="container">
        <h1>Perbandingan Angka</h1>

        <div class="result">
            <span class="label"><?php echo $a; ?> == <?php echo $b; ?>:</span>
            <span class="value"><?php echo ($a == $b) ? 'True' : 'False'; ?></span>
            <br>
            <span class="label"><?php echo $a; ?> != <?php echo $b; ?>:</span>
            <span class="value"><?php echo ($a != $b) ? 'True' : 'False'; ?></span>
            <br>
            <span class="label"><?php echo $a; ?> > <?php echo $b; ?>:</span>
            <span class="value"><?php echo ($a > $b) ? 'True' : 'False'; ?></span>
            <br>
            <span class="label"><?php echo $a; ?> < <?php echo $b; ?>:</span>
            <span class="value"><?php echo ($a < $b) ? 'True' : 'False'; ?></span>
            <br>
            <span class="label">(<?php echo $a; ?> == <?php echo $b; ?>) && (<?php echo $a; ?> > <?php echo $b; ?>):</span>
            <span class="value"><?php echo (($a == $b) && ($a > $b)) ? 'True' : 'False'; ?></span>
            <br>
            <span class="label">(<?php echo $a; ?> == <?php echo $b; ?>) || (<?php echo $a; ?> > <?php echo $b; ?>):</span>
            <span class="value"><?php echo (($a == $b) || ($a > $b)) ? 'True' : 'False'; ?></span>
        </div>
    </div>
</body>
</html>